from osr_reader import *
from osu_reader import *
from math import sqrt, log2, atan2, pi
import sys

def osr_obj_distance(objs):
    o1, o2 = objs
    if not isinstance(o1, HitCircle) or not isinstance(o2, (HitCircle, Slider)):
        raise Exception("wrong argument types")
    return sqrt((o1.x-o2.x)**2 + (o1.y-o2.y)**2)

def dist(x1,y1,x2,y2):
    return sqrt((x1-x2)**2+(y1-y2)**2)


if len(sys.argv) < 3:
    print('usage: map.osu replay1.osr replay2.osr replay3.osr')
    sys.exit(0)


#p = '/home/phone/studienarbeit/'
#omap = p+'mv.osu'
#oreplay = p+'mv.osr'
omap = sys.argv[1]
print("using map"+str(omap))

maxx = 512
maxy = 384

m = OsuData(omap)

prev = None
jumps = []
jumps_hr = []
circle_radius = (54.4 - 4.48 * float(m.difficulty['CircleSize']))
circle_radius_hr = (54.4 - 4.48 * float(m.difficulty['CircleSize'])*1.3)
for i in sorted(m.hit_objects):
    curr = m.hit_objects[i]
    if isinstance(prev, HitCircle) and isinstance(curr, (HitCircle, Slider)):
        if 0 < curr.start_time - prev.start_time < 2000:
            if osr_obj_distance((prev, curr)) > 2*circle_radius:
                jumps.append((prev,m.hit_objects[i]))
            if osr_obj_distance((prev, curr)) > 2*circle_radius_hr:
                jumps_hr.append((prev,m.hit_objects[i]))
        else:
            pass
            #print(curr.start_time - prev.start_time)
    prev = m.hit_objects[i]

odata = []
for oreplay in sys.argv[2:]:
    print('analyzing replay '+str(oreplay))
    r = OsrData(oreplay)

    dt = r.is_dt()
    hr = r.is_hr()

    if hr:
        jmps = jumps_hr
        cs = circle_radius_hr
    else:
        jmps = jumps
        cs = circle_radius

    result = []
    for i in jmps:
        o1, o2 = i
        t1 = o1.start_time
        t2 = o2.start_time
        keys = list(filter(lambda x: t1<=x<=t2+40, r.time_map.keys()))
        keys.sort()
        cursor_points = [[k]+r.time_map[k] for k in keys]
        d = dist(o1.x,o1.y,o2.x,o2.y)-2*cs
        t = filter(lambda x: dist(o2.x, o2.y, x[1], x[2]) < cs, cursor_points)

        try:
            first_good_position = min(t, key=lambda x: x[0])
            if first_good_position[0]-t1 < 0: continue

        except:
            continue
            pass
            #print(o1, o2, cs, '\n\n\n\n\n\n\n\n\n')

        l_dist = d
        l_time_needed = (first_good_position[0]-t1) / (1.5 if dt else 1)
        l_max_time = (t2-t1) / (1.5 if dt else 1)
        l_radius = cs
        l_area = pi*(l_radius**2)
        l_angle = atan2(o2.y-o1.y, o2.x-o1.x)*180/pi
        l_hr = hr
        l_dt = dt
        l_dist_p_area_log = log2(l_dist / l_area)
        l_dist_p_area = l_dist/l_area
        l_dist_p_radius_log = log2(l_dist / l_radius)
        l_dist_p_radius = l_dist/l_radius
        l_time_p_max_time = l_time_needed / l_max_time
        l_dist_p_time = l_dist / l_time_needed
        l_dist_p_max = l_dist / l_max_time

        odata.append([
l_dist ,
l_time_needed ,
l_max_time ,
l_radius ,
l_area,
l_angle,
l_hr ,
l_dt,
l_dist_p_area_log,
l_dist_p_area,
l_dist_p_radius_log,
l_dist_p_radius,
l_time_p_max_time,
l_dist_p_time,
l_dist_p_max ])



#header = ["l_time_needed", "l_dist" "l_max_time", "l_radius", "l_area", "l_log_dist_p_area", "l_log_dist_p_radius", "l_angle", "l_hr", "l_dt"]
#header = ["distance", "time_needed", "max_time", "time_left", "circle_radius", "target_area", "angle", "HR", "DT", "log(dist/area)"]

f = open('data.csv', 'w')

for j in odata:
    f.write(str(j).replace('[','').replace(']',''))
    f.write('\n')


#for i in range(7):
#    f = open('sorted_by_'+str(i)+'.csv', 'w')
#
#    f.write(str(header).replace('[','').replace(']',''))
#    f.write('\n')
#
#    for j in sorted(odata, key=lambda a:a[i]):
#        f.write(str(j).replace('[','').replace(']',''))
#        f.write('\n')
