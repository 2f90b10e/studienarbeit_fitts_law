
class HitObject:
    def __init__(self, x=0, y=0, time=0, endline=''):
        self.x = x
        self.y = y
        self.start_time = time
        self.end_time = time 
        self.path = [(x,y)]
        self.parse_endline(endline)

    def parse_endline(self, endline):
        raise Exception('parse_endline() needs to be implemented')

class HitCircle(HitObject):
    def parse_endline(self, endline):
        self.line = endline
        pass

class Slider(HitObject):
    def parse_endline(self, endline):
        self.line = endline
        pass

class Spinner(HitObject):
    def parse_endline(self, endline):
        self.line = endline
        pass


class OsuData:
    def parse_general(self, line):
        key, value = line.split(':', maxsplit=1)
        self.general[key] = value

    def parse_editor(self, line):
        key, value = line.split(':', maxsplit=1)
        self.editor[key] = value
        
    def parse_metadata(self, line):
        key, value = line.split(':', maxsplit=1)
        self.metadata[key] = value

    def parse_difficulty(self, line):
        key, value = line.split(':', maxsplit=1)
        self.difficulty[key] = value

    def parse_events(self, line):
        #print('event ignored:', line)
        #print('cannot parse events', line)
        pass

    def parse_timing_points(self, line):
        #print(line)
        #print(self.timing_points)
        offset, ms_per_beat, rest = line.split(',', maxsplit=2)
        offset = int(offset)
        ms_per_beat = float(ms_per_beat)
        inherited = ms_per_beat < 0
        if inherited:
            base = list(filter(lambda k: k<=offset and self.timing_points[k][1] == False, self.timing_points))[-1]
            ms_per_beat = base*(-ms_per_beat/100)

        while float(offset) in self.timing_points:
            offset = float(offset)+1/2**4
        self.timing_points[float(offset)] = (ms_per_beat, inherited)

    def parse_colors(self, line):
        #print('cannot parse colors', line)
        pass

    def parse_hit_objects(self, line):
        try:
            pos_x,pos_y,hit_time,hit_type,hit_sound,rest = line.split(",", maxsplit=5)
        except:
            pos_x,pos_y,hit_time,hit_type,hit_sound = line.split(",")
            rest = ""
        hit_time = int(hit_time)
        hit_type = int(hit_type) & 0x0B
        pos_x = int(pos_x)
        pos_y = int(pos_y)
        if hit_type == 1: # hit circle
            self.hit_objects[hit_time] = HitCircle(x=pos_x, y=pos_y, time=hit_time)
        elif hit_type == 2: # slider
            self.hit_objects[hit_time] = Slider(pos_x, pos_y, hit_time, rest)
        elif hit_type == 8: # spinner
            self.hit_objects[hit_time] = Spinner(pos_x, pos_y, hit_time, rest)
        else:
            raise Exception('unexpected hit object type', hit_type)

    def parse_global(self, line):
        #print('not parsing globals.', line)
        pass

    def __init__(self, filename=None):
        self.general = {}
        self.editor = {}
        self.metadata = {}
        self.difficulty = {}
        self.events = {}
        self.timing_points = {}
        self.colors = {}
        self.hit_objects = {}
        if filename:
            self.load(filename)

    def load(self, filename):
        with open(filename, 'rb') as f:
            data = f.read().decode('utf-8')
            parser = self.parse_global
            for line in data.splitlines():
                if line == '[General]':
                    parser = self.parse_general
                elif line == '[Editor]':
                    parser = self.parse_editor
                elif line == '[Metadata]':
                    parser = self.parse_metadata
                elif line == '[Difficulty]':
                    parser = self.parse_difficulty
                elif line == '[Events]':
                    parser = self.parse_events
                elif line == '[TimingPoints]':
                    parser = self.parse_timing_points
                elif line == '[Colours]':
                    parser = self.parse_colors
                elif line == '[HitObjects]':
                    parser = self.parse_hit_objects
                elif len(line) == 0:
                    continue
                else:
                    parser(line)


