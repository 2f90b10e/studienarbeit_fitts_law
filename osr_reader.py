#osr parser reader

import struct 
import lzma


def osr_extract_uleb(data):
    num_bytes = 0
    value = 0
    extracted_byte = 0xFF
    while extracted_byte & 0x80 != 0:
        extracted_byte, = struct.unpack_from('<B', data)
        data = data[1:]
        value += (extracted_byte & 0x7F) * (2**(num_bytes*7))
        num_bytes += 1
    return value, data


def osr_extract_string(data):
    string_type, = struct.unpack_from('<B', data)
    data = data[1:]
    if string_type == 0x00: # empty string 
        return "", data
    if string_type == 0x0B: # string 
        str_len, data = osr_extract_uleb(data)
        str_value, = struct.unpack_from('<'+str(str_len)+'s', data)
        return str_value, data[str_len:]
    raise Exception('unexpected value while decoding string', data[:10])


class OsrData:
    def __init__(self, filename=None):
        self.data = None
        self.mode = None
        self.mode = None
        self.version = None
        self.beatmap_hash = None
        self.player_name = None
        self.replay_hash = None
        self.num_300 = None
        self.num_50 = None
        self.num_geki = None
        self.num_katu = None
        self.num_miss = None
        self.score = None
        self.combo = None
        self.perfect = None
        self.mods = None
        self.life_bar = None
        self.time_stamp = None
        self.data_size = None
        self.data = None
        self.score_id = None
        self.time_map = {}
        if filename:
            self.load(filename)

    def is_dt(self):
        return (self.mods & (1<<6)) != 0

    def is_hr(self):
        return (self.mods & (1<<4)) != 0


    def load(self, filename):
        with open(filename, 'rb') as f:
            data = f.read()
            self.mode, \
            self.version = struct.unpack_from('<BI', data)
            data = data[5:]

            self.beatmap_hash, data = osr_extract_string(data)
            self.player_name, data = osr_extract_string(data)
            self.replay_hash, data = osr_extract_string(data)

            self.num_300, \
            self.num_100, \
            self.num_50,  \
            self.num_geki, \
            self.num_katu, \
            self.num_miss, \
            self.score, \
            self.combo, \
            self.perfect, \
            self.mods = struct.unpack_from('<HHHHHHIHBI', data)
            data = data[23:]

            self.life_bar, data = osr_extract_string(data)

            self.time_stamp, self.data_size = struct.unpack_from('<QI', data)
            data = data[12:]

            self.data, self.score_id = struct.unpack_from('<'+str(self.data_size)+'sQ', data)
            data = data[self.data_size+8:]

            if len(data) > 0:
                print('leftover data after parsing osr file')

            self.data = lzma.decompress(self.data)
            self.create_time_map()

    def create_time_map(self):
        if not self.data:
            return 
        self.time_map = {}
        curr_time = 0
        events = self.data.split(b',')[:-1]
        for event in events:
            try:
                time_delta, pos_x, pos_y, state = event.split(b'|')
                if int(time_delta) == -12345: continue
                curr_time += int(time_delta)
                if self.time_map.get(curr_time):
                    pass
                    #print(curr_time, self.time_map.get(curr_time), event)
                if not self.is_hr():
                    self.time_map[curr_time] = [float(pos_x), float(pos_y), int(state)]
                else:
                    self.time_map[curr_time] = [float(pos_x), 384-float(pos_y), int(state)]
            except:
                print('error in handling',event)
                continue
        
        



